#
# Research CLI for replicating research and modeling new data
#
# Goal: CLI main methods
#   replicate() - this will run the entire modeling process. From data
#       consumption and transformation to modeling and test set prediction. It
#       will end by spitting out validation metrics and
#       hyper parameter settings. Will use local dataset if exists.
#
#       options: provide data visualizations, verbose logging,
#           docker (run in container), fetch (force re-fetch of dataset)
#
#   gather_data() - this will retrieve new data that can be used for training
#       or more prediction
#
#       options: store (persist data), predict
#

import argparse

from toml import loads as load_toml


class CLIArgs(object):
    """
    Arg parser creator
    """

    _RESEARCH_FUNCS = ['replicate', 'gather_data', 'predict']

    description = 'CLI for research projects. Reproduce research, ' \
                  'gather new data, predict new responses'

    cli_args = [{
        'name': 'func',
        'kwargs': {
            'action': 'store',
            'choices': _RESEARCH_FUNCS,
            'help': 'Research function to execute.\n\n'
                    'replicate: load, transform and model a project. '
                    'Providing summary statistics.\n\n'
                    'gather_data: retrieve more data for given project.\n\n'
                    'predict: predict on data for given project',
            'default': 'replicate'
        }
    }, {
        'name': 'project',
        'kwargs': {
            'action': 'store',
            'help': 'Research project to run function on'
        }
    }, {
        'flags': ['--visuals', '-v'],
        'kwargs': {
            'action': 'store_true',
            'dest': 'with_visuals',
            'help': 'Provide graphics compiled during research function'
        }
    }, {
        'flags': ['--log-level', '-l'],
        'kwargs': {
            'action': 'store',
            'dest': 'log_level',
            'help': 'Log level for stdout',
            'default': 'info',
            'choices': ['debug', 'info', 'warn', 'error']
        }
    }, {
        'flags': ['--docker'],
        'kwargs': {
            'action': 'store_true',
            'dest': 'docker',
            'help': 'Run research func in a docker container'
        }
    }, {
        'name': '--fetch',
        'kwargs': {
            'action': 'store_true',
            'dest': 'fetch',
            'help': 'Force fetch of new data for function.'
        }
    }]

    def __init__(self, config):
        self.conf = config
        self.supported_projects = config['projects']['supported']
        self.parser = self._get_parser()

    @classmethod
    def _get_parser(cls):
        parser = argparse.ArgumentParser(description=cls.description)

        for cli_arg in cls.cli_args:
            names_or_flags = (
                [cli_arg['name']] if 'name' in cli_arg else cli_arg['flags'])
            parser.add_argument(*names_or_flags, **cli_arg['kwargs'])

        return parser

    def parse_args(self):
        args = self.parser.parse_args()
        print(args)
        project = args.project
        if project not in self.supported_projects:
            raise argparse.ArgumentError(
                argument='project',
                message='Invalid project %s. Should be one of %s' % (
                    project, ",".join(self.supported_projects)))

        return args


def load_research_config(file_name):
    with open(file_name) as conf:
        return load_toml(conf.read())


def parse_config(config_dict):
    return {}
