import os

from cli.args import load_research_config, CLIArgs


_DEFAULT_CONF = 'conf.toml'
CONFIG_FILE = os.getenv('CONFIG_FILE', _DEFAULT_CONF)


def _main(args):
    pass


if __name__ == '__main__':
    config = load_research_config(CONFIG_FILE)
    parser = CLIArgs(config)
    args = parser.parse_args()
    _main(args)
