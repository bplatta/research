import pandas as pd
from sklearn.preprocessing import scale


def strip_after_x_columns(df, x):
    """
    Remove columns after ord X
    :param df: dataframe
    :param x: integer
    :return: dataframe
    """
    return df[list(range(1, x+1))]


def strip_text_col(df, col):
    """
    Call str.strip() on column
    :param df: dataframe
    :param col: str
    :return: dataframe
    """
    df[col] = df[col].apply(lambda s: s.strip())
    return df


def convert_index_to_dt(df):
    """
    Convert current index to a datetime index
    (assume str is proper datetime format)
    :param df: dataframe
    :return: dataframe
    """
    return df.set_index(pd.DatetimeIndex(df.index))


def set_range_index(df):
    """
    Set index of dataframe to simple range index
    :param df: dataframe
    :return: dataframe
    """
    return df.set_index(pd.RangeIndex(1, df.shape[0] + 1))


def convert_to_category(df, *cols):
    """
    Convert passed columns *args to pd.Category objects
    :param df: dataframe
    :param cols: *<str>
    :return: dataframe
    """
    for col in cols:
        df[col] = df[col].astype('category')
    return df


def aggregate_category_OR(series):
    """
    Logical _OR_ for dummy variable category series
    :param series: dataframe
    :return: integer
    """
    return 1 if 1 in set(series) else 0


def get_dummies_for_category(df, category):
    """
    Get dummy matrix for dataframe category column
    :param df: dataframe
    :param category: str
    :return: dataframe
    """
    return pd.get_dummies(df[category])


def inner_join_on_index(*datasets):
    """
    Merge datasets on their indexes. = SQL Inner join
    :param datasets: *[dataframes, ...]
    :return: dataframe
    """
    return pd.merge(*datasets, right_index=True, left_index=True)


def fill_column(df, col, fill_value):
    """
    Update dataframe, filling {col} NAN or NULL values with {fill_value}
    :param df: dataframe
    :param col: str
    :param fill_value: Any
    :return: dataframe
    """
    df[col] = df[col].fillna(fill_value)
    return df


def log_state(df, logger, message, stage):
    """
    Log current shape of the dataframe and message
    :param df: dataframe
    :param logger: python Logger object
    :param message: str
    :param stage: logger.STAGES object
    :return: dataframe
    """
    logger.info('shape %s: %s' % (df.shape, message), stage=stage)
    return df


def normalize_column(df, column):
    """
    Scale column with standard normal transformation: (col - {mean}) / {std}
    :param df: dataframe
    :param column: str
    :return: Dataframe
    """
    mean = df[column].mean()
    std = df[column].std()
    df[column] = (df[column] - mean) / std
    return df


def with_columns(df, columns_to_keep):
    """
    Drop columns not to be retained
    :param df: dataframe
    :param columns_to_keep: list or set of columns to retain
    :return: dataframe
    """
    return df.drop(list(set(df.columns) - set(columns_to_keep)), axis=1)
