import os
import zipfile
import subprocess as sub_p
import pandas as pd
from functools import partial

from utils.exceptions import DataLoadError

DATASET_DIR = os.getenv('DATA_DIR', 'datasets')
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def path_to(rel_dir_path, file_or_dir):
    """
    Constructs path to file
    :param rel_dir_path: relative path to directory from project root
    :param file_or_dir:
    :return: str
    """
    dir_path = os.path.join(BASE_DIR, rel_dir_path)
    return os.path.join(dir_path, file_or_dir)

path_to_datasets = partial(path_to, DATASET_DIR)


def stat_dir_or_file(path):
    """
    Stats for file or dir
    :param path:
    :return:
    """
    try:
        os.stat(path)
        return True
    except OSError:
        return False


def mk_dir(dir_path):
    """
    Creates local directory
    :param dir_path:
    :return:
    """
    dir_name = dir_path.split('.')[0]  # split if file included
    sub_p.Popen(['mkdir', '-p', dir_path])
    return dir_path


def unzip(full_path_archive, dir_path=None):
    """
    Unzip file to directory of the same name
    :param full_path_archive: str
    :param dir_path: str or None
    """
    if not stat_dir_or_file(full_path_archive):
        raise DataLoadError(
            '%s not found in %s' % (full_path_archive, DATASET_DIR))
    # call unzip to path
    zipfile.ZipFile(full_path_archive).extractall(path=dir_path)

    return dir_path


def clean_up(path):
    """
    Remove data at path
    :param path: str
    :return: Bool
    """
    sub_p.Popen(['rm', '-rf', path])
    return True


def load_csv_into_dataframe(*csv_rel_path, **kwargs):
    return pd.read_csv(
        "/".join(csv_rel_path), **kwargs).rename(columns=str.lower)
