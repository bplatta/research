# About bplatta/research
Goal is to provide a platform for easily reproducing any research or data projects I do.

## Taxonomy
I've created a new taxonomy based on pandas/sklearn functionality that is a little
more intuitive for me:  

`/explore/`    - visualization/graphing  
`/fetch/`      - Retrieving datasets  
`/language/`   - nlp code  
`/load/`       - for loading and transforming datasets into workable dataframes  
`/model/`      - for creating and finalizing a model. (hyperparameter tuning, cross-validation etc.)  
`/notes/`      - markdown notes on ML and data gathering etc.  
`/run/`        - docker  
`/scripts/`    - one off python scripts  

## Process
1. Grab dataset. Automate in `/fetch/` if needed.
2. Load and visualize to understand the data, form hypothesis (manual) `/explore/`
3. Transform data into workable ML data structures `/load/`
4. Test out various ML algorithms depending on questions asking of dataset
5. Validate on test sets, tune hyper parameters, and serialize and store `/model/`

Note 2-5 will be a fairly iterative process.

## `research.toml`
`research_cli.py` uses this configuration file for replicating research. From data gathering to test set prediction.

Run `python research_cli.py --help` for more information.

