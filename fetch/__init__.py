import requests

from urllib.parse import urlencode


def encode_query(query_arg_dict):
    return urlencode(query_arg_dict)


def fetch_raw(endpoint, **query_args):
    q_str = '' if query_args == {} else encode_query(query_args)
    final_endpoint = endpoint if q_str == '' else '%s?%s' % (endpoint, q_str)
    return requests.get(final_endpoint)


def validate_status(expected_status):
    return lambda res: res.status_code == expected_status


def kaggle_download(path_to_file, url, **creds):
    """
    Download from Kaggle - requires credentials
    :param path_to_file: str
    :param url: str
    :param creds: {}
    :return:
    """
    req_1 = requests.get(url)
    return write_stream(
        requests.post(req_1.url, data=creds, stream=True),
        path_to_file)


def download(path_to_file, url, chunk_size=512):
    """
    Download file from location
    :param path_to_file: str - full path to file str
    :param url: str
    :param byte_chunk_size: int
    :return: str
    """
    return write_stream(
        requests.get(url, stream=True), path_to_file, chunk_size)


def write_stream(response, path_to_file, chunk_size=512):
    with open(path_to_file, 'wb') as data:
        for chunk in response.iter_content(chunk_size):
            if chunk:
                data.write(chunk)
    return path_to_file
