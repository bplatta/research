import logging
import sys

LOG_FORMAT = '[%(asctime)s] [%(stage)s] [%(levelname)s] %(message)s'


class _Stage:
    LOAD = {'stage': 'load'}
    TRANSFORM = {'stage': 'transform'}
    RUN_MODEL = {'stage': 'run-model'}
    NONE = {'stage': '_'}

STAGES = _Stage()


def _get_logger(name=None):
    logger = logging.getLogger((name or __name__))
    logger.setLevel('DEBUG')
    stdout_handler = logging.StreamHandler(stream=sys.stdout)
    stdout_handler.setFormatter(logging.Formatter(LOG_FORMAT))
    stdout_handler.setLevel('DEBUG')
    logger.addHandler(stdout_handler)
    return logger


class ResearchLogger(object):
    def __init__(self, name):
        self.logger = _get_logger(name)
        self.cur_stage = STAGES.NONE

    def as_stage(self, stage):
        self.cur_stage = stage
        return self

    def info(self, message, stage=None):
        self.logger.info(message, extra=(stage or self.cur_stage))

    def warn(self, message, stage=None):
        self.logger.warn(message, extra=(stage or self.cur_stage))

    def error(self, message, stage=None):
        self.logger.error(message, extra=(stage or self.cur_stage))

