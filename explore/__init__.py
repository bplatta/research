import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from scipy import stats

from explore.defaults import DEF_FIG_SIZE, GRAPH_CONSTANTS


def display_stacked_bar(data_frame, figure_size=DEF_FIG_SIZE):
    """
    Display a simple stacked bar graph
    :param data_frame:
    :param figure_size:
    :return:
    """
    figure = plt.figure(figsize=figure_size)
    data_frame.plot(
        kind=GRAPH_CONSTANTS.BAR,
        stacked=True,
        figsize=figure_size
    )

    return figure.show()


def display_hist(
        data_frame, bins, labels, legend=None, figure_size=DEF_FIG_SIZE):

    figure = plt.figure(figsize=figure_size)
    plt.hist(
        data_frame, stacked=True, color=['g', 'r'], bins=bins, label=legend)

    plt.xlabel(labels[0])
    plt.ylabel(labels[1])
    plt.legend()
    return figure.show()


def scatter_plot(data_frames, labels, legend=None, figure_size=DEF_FIG_SIZE):
    scalar = 40

    figure = plt.figure(figsize=figure_size)
    ax = plt.subplot()
    for frame in data_frames:
        ax.scatter(frame, s=scalar)

    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    if legend:
        ax.legend(legend, scatterpoints=1, loc='upper right', fontsize=15)

    return figure.show()
