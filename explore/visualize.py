import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from scipy import stats
from pandas import DataFrame


class Visualizer(object):
    """
    Class that makes the visualization library a little more human readable
    """
    DEF_FIG_SIZE = (15, 8)
    HEXBIN = 'hex'

    _ax_map = {
        'histogram': 'hist',
        'hexagon_bin': HEXBIN,
        'density_estimator': 'kde',
        'obs_ticks': 'rug'
    }

    _defaults_univariate = {
        'hist': True,
        'kde': False,
        'rug': False
    }

    def __init__(self, dataframe=None, figsize=None):
        self.df = dataframe
        self.figuresize = figsize or self.DEF_FIG_SIZE
        self._kwargs = {}

    @property
    def cols(self):
        return self.df.columns

    @property
    def options(self):
        print('Overlays for analysis: %s' % ",".join(self._ax_map.keys()))

    def set_labels(self, ax, x, y):
        if x:
            ax.set_xlabel(x)
        if y:
            ax.set_ylabel(y)
        return self

    def rotate_axis_labels(self, ax, axis='x', rotation=45):
        set_labels = getattr(ax, 'set_%sticklabels' % axis)
        get_labels = getattr(ax, '%saxis' % axis).get_ticklabels

        set_labels(get_labels(), rotation=rotation)
        return self

    def df_or_cls(self, df):
        return df if isinstance(df, DataFrame) else self.df

    def with_dataframe(self, df):
        self.df = df

    def show(self):
        plt.show()
        self.reset()

    def close(self):
        plt.close()

    def reset(self):
        self._kwargs = {}
        return self

    def _with_kwarg(self, k, v):
        self._kwargs[k] = v
        return self

    def bin(self, bins):
        return self._with_kwarg('bins', bins)

    def with_ax(self, kind):
        return self._with_kwarg(self._ax_map.get(kind), True)

    #
    # Univariate modeling
    #

    def univariate(self, column):
        kwargs = {**self._defaults_univariate, **self._kwargs}
        self.reset()
        sns.distplot(self.df[column], **kwargs)
        return self

    def density_estimate(self, col):
        self.reset()
        sns.kdeplot(self.df[col], shade=True)
        return self

    #
    # Bivariate distribution modeling
    #

    def scatter_plot(self, col_x, col_y):
        sns.jointplot(x=col_x, y=col_y, data=self.df)
        return self

    def hexbin(self, col_x, col_y, **kwargs):
        sns.jointplot(x=col_x, y=col_y, data=self.df, kind=self.HEXBIN)
        return self

    def scatter_strip_plot(self, category_col, col_y, spread=False):
        sns.stripplot(x=category_col, y=col_y, data=self.df, jitter=spread)
        return self

    def stacked_bar(self, df=None, xlabel=None, ylabel=None):
        ax = self.df_or_cls(df).plot.bar(stacked=True)
        self.set_labels(ax, xlabel, ylabel).rotate_axis_labels(ax)
        return self


def count_group_size(df, group_cols):
    """
    Group by and count size of each group
    :param df: dataframe
    :param group_cols: list or str
    :return: Series
    """
    return df.groupby(group_cols).size()
