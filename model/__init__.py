from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import (
    cross_val_score, GridSearchCV, RandomizedSearchCV)


def search_hyperparam_space(estimator, param_grid, parallel=False):
    n_jobs = 1 if not parallel else -1
    return GridSearchCV(
        estimator=estimator, param_grid=param_grid, n_jobs=n_jobs)


class Classify:
    SVC = svm.SVC
    LogisticRegression = LogisticRegression

    _default_grid_search = {
        'SVC': [{
            'kernel': ['rbf'],
            'gamma': [1e-3, 1e-4],
            'C': [1, 10, 100, 1000]
        }, {
            'kernel': ['linear'],
            'C': [1, 10, 100, 1000]
        }]
    }


class Metrics:
    pass
