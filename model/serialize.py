import pickle


def serialize(model):
    return pickle.dumps(model)


def deserialize(model_serial):
    return pickle.loads(model_serial)