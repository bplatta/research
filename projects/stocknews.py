from functools import partial
from nltk import word_tokenize

from logger import ResearchLogger, STAGES
from language.keyword_finders.countries import Countries
from language.sentiment import LexicalBasedScorer, categorize_score
from load import unzip, mk_dir, load_csv_into_dataframe, path_to_datasets
from load.pipeline import (
    convert_index_to_dt, strip_text_col, convert_to_category,
    get_dummies_for_category, aggregate_category_OR,
    inner_join_on_index, fill_column, log_state)
from model import Classify, search_hyperparam_space

ZIP_FILE = path_to_datasets('stocknews.zip')
DATA_DIR = path_to_datasets('stocknews')
DJIA_CSV = 'DJIA_table.csv'
REDDIT_CSV = 'RedditNews.csv'
LEXICAL_SCORER = 'afinn'


logger = ResearchLogger(__name__)

#
# 1. Clean and merge djia and reddit news headline datasets
# 2. Parse headlines and do some basic sentiment analysis / topic analysis
# 3. Using nlp metrics, create model for
#    predicting whether market ends up or down
#


def map_headline_to_country(country_keywords, headline):
    """
    Split headline and check if country keyword in tokens
    :param country_keywords: language
    :param headline:
    :return:
    """
    # simplest tokenization for now
    headline_parts = word_tokenize(headline)
    for candidate in headline_parts:
        if not candidate:
            continue

        country = country_keywords.find(candidate)
        if country:
            return country
    return None


def score_sentiment_and_group(dataframe):
    """
    Group reddit by date and aggregate variables, scoring headline sentiment
    :param dataframe: pandas DF
    :return: DF
    """
    logger.info('Grouping Reddit data by date', stage=STAGES.TRANSFORM)

    # Sum all sentiment scores for the day
    sentiment_series = dataframe.groupby(by=dataframe.index).sentiment.sum()

    # Convert country category variable to matrix of dummy variables
    # and group by date, code to 1 if any of group contain dummy
    country_matrix = get_dummies_for_category(dataframe, 'country')
    country_matrix = country_matrix.groupby(
        by=country_matrix.index, as_index=False
    ).agg([aggregate_category_OR])
    # Reset columns to original, aggregate creates Multi-index
    country_matrix.columns = country_matrix.columns.get_level_values(0)

    # Do similarly for country and sentiment interaction variable
    country_sentiment_matrix = get_dummies_for_category(
        dataframe, 'country_AND_sentiment')
    country_sentiment_matrix = country_sentiment_matrix.groupby(
        by=country_sentiment_matrix.index).agg([aggregate_category_OR])
    country_sentiment_matrix.columns = country_sentiment_matrix\
        .columns.get_level_values(0)

    logger.info(
        'Reddit category dummy variables created', stage=STAGES.TRANSFORM)
    return inner_join_on_index(
        country_matrix, country_sentiment_matrix).assign(
        sentiment=sentiment_series)


def build_reddit_dataset():
    """
    Map raw reddit data to dataset
    :return: DF
    """
    logger.info('Loading Reddit data', stage=STAGES.LOAD)

    # Language parsing and scoring methods
    lexical_scorer = LexicalBasedScorer(type=LEXICAL_SCORER)
    country_keywords = Countries()
    map_to_country = partial(map_headline_to_country, country_keywords)
    # Load -> add new columns -> pivot on date -> remove mostly empty columns
    # -> add back date column as non-index column -> index with integers
    # Takes a minute
    return load_csv_into_dataframe(DATA_DIR, REDDIT_CSV,
                                   parse_dates=True,
                                   infer_datetime_format=True)\
        .pipe(strip_text_col, 'news')\
        .set_index('date')\
        .pipe(convert_index_to_dt)\
        .pipe(
            log_state, logger,
            'Datetime index created. Adding columns.', STAGES.TRANSFORM)\
        .assign(sentiment=lambda frame: frame.news.map(lexical_scorer.score))\
        .assign(country=lambda frame: frame.news.map(map_to_country))\
        .assign(sentiment_cat=lambda df: df.sentiment.map(categorize_score))\
        .pipe(fill_column, 'country', 'none')\
        .pipe(convert_to_category, 'country', 'sentiment_cat')\
        .assign(country_AND_sentiment=(
            lambda df: (
                df.country.astype('str') + '_' + df.sentiment_cat.astype('str')
            )))\
        .pipe(convert_to_category, 'country_AND_sentiment')\
        .pipe(
            log_state, logger,
            'Category columns created.', STAGES.TRANSFORM)\
        .drop(['sentiment_cat'], axis=1)


def build_djia_dataset():
    logger.info('Loading and transforming DJIA data', stage=STAGES.LOAD)
    djia_columns = ['up']
    return load_csv_into_dataframe(DATA_DIR, DJIA_CSV,
                                   parse_dates=True,
                                   infer_datetime_format=True)\
        .assign(on_close_diff=lambda row: row['adj close'] - row.open)\
        .set_index('date')\
        .pipe(convert_index_to_dt)\
        .assign(up=(
            lambda df: df.on_close_diff.map(
                lambda d: 1 if d > 0 else 0)
            )
        )[djia_columns]


def get_merged_dataset():
    """
    Merge both datasets with an inner join
    :return: Dataframe
    """
    return inner_join_on_index(
        score_sentiment_and_group(build_reddit_dataset()),
        build_djia_dataset())


def model(X, Y):
    param_grid = {
        'kernel': ['rbf'],
        'C': [1, 10, 100, 1000],
        'gamma': [0.1, 0.01, 0.001, 0.0001]
    }

    from sklearn.model_selection import StratifiedKFold

    three_fold = StratifiedKFold(n_splits=3)
    classifer = search_hyperparam_space(Classify.SVC(), param_grid)
    logger.as_stage(STAGES.RUN_MODEL)
    logger.info('Run grid search and scoring models')

    for train_index, test_index in three_fold.split(X, Y):
        score = classifer\
            .fit(X[train_index], Y[train_index])\
            .score(X[test_index], Y[test_index])
        logger.info('Score: %s' % score)


def get_X(data):
    return data.drop('up', axis=1).values


def get_Y(data):
    return data.up.values


def main():
    """
    MAIN method: load, transform, model and predict
    """
    logger.as_stage(STAGES.LOAD)
    logger.info('Running stocknews script')
    logger.info('Unzipping stocknews archive')
    unzip(ZIP_FILE, mk_dir(DATA_DIR))
    data = get_merged_dataset()
    logger.as_stage(STAGES.TRANSFORM)
    logger.info('Splitting data variables')
    X = get_X(data)
    Y = get_Y(data)
    model(X, Y)
    return X, Y


if __name__ == '__main__':
    main()
