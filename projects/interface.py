def run(project, args):

    Load = project.load
    Transform = project.transform
    Model = project.model

    model  = Model(Transform(Load()))


class ProjectInterface(object):
    DATA_DIR = None
    ARCHIVE = None
    URL = None
    DATA_URL = None

    def about(self):
        info = getattr(self, '_about', {})

        _def_val = ''
        print('Goal: ', info.get('goal', _def_val))
        print('Description: ', info.get('description', _def_val))
        print('Conclusion: ', info.get('conclusion', _def_val))



    def load(self):
        raise NotImplementedError()

    def transform(self, dataframe):
        raise NotImplementedError()

    def model(self):
        raise NotImplementedError()

    def visualize(self, *args, **kwargs):
        raise NotImplementedError()

    def predict(self, cln_df):
        raise NotImplementedError()

    def gather_data(self):
        raise NotImplementedError()