import pandas as pd

from logger import ResearchLogger, STAGES
from load import (
    unzip, mk_dir, path_to_datasets, load_csv_into_dataframe)
from load.pipeline import convert_index_to_dt, strip_text_col, strip_after_x_columns

ZIP_FILE = path_to_datasets('kickstarter-project-statistics.zip')
DATA_DIR = path_to_datasets('kickstarter-stats')
MOST_BACKED_CSV = 'most_backed.csv'

logger = ResearchLogger(__name__)


def load():
    return load_csv_into_dataframe(DATA_DIR, MOST_BACKED_CSV)


def transform(dataframe):
    return dataframe.drop('unname: 0', 1)\
        .rename(columns=lambda c: c.replace('.', '_'))


def main():
    logger.info('Running kickstarter script script')
    logger.as_stage(STAGES.LOAD).info('Unzipping kickstarter archive')
    unzip(ZIP_FILE, mk_dir(DATA_DIR))
    return False


if __name__ == '__main__':
    main()
