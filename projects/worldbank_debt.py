from projects.interface import ProjectInterface

from fetch import download
from load import (
    path_to_datasets, unzip, mk_dir, clean_up, stat_dir_or_file,
    load_csv_into_dataframe)
from logger import ResearchLogger


class WorldBankProject(ProjectInterface):
    """
    World Bank data on international debt. just exploring for now
    """

    DATA_DIR = path_to_datasets('worldbank-debt')
    ARCHIVE = path_to_datasets('worldbank-debt.zip')
    URL = 'https://www.kaggle.com/theworldbank/' \
          'international-debt-statistics'
    DATA_URL = 'http://databank.worldbank.org/data/download/IDS_CSV.zip'

    def __init__(self):
        self.data_exists = stat_dir_or_file(self.DATA_DIR)
        self.archive_exists = stat_dir_or_file(self.ARCHIVE)

    def fetch(self):
        download(path_to_datasets(self.ARCHIVE), self.DATA_URL)

    def unzip(self):
        unzip(self.ARCHIVE, mk_dir(self.DATA_DIR))

    def clean_up(self):
        return clean_up(self.DATA_DIR) and clean_up(self.ARCHIVE)

    def load(self, fetch=False):
        if fetch:  # Force fetching of data
            self.clean_up()
            self.fetch()
            self.unzip()

        if not self.data_exists:
            if not self.archive_exists:
                self.fetch()
            self.unzip()

        return True

    def load_csv(self, csv, **kwargs):
        return load_csv_into_dataframe(self.DATA_DIR, csv, **kwargs)
