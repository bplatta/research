import getpass
from collections import namedtuple

from projects.interface import ProjectInterface

from explore.visualize import Visualizer, count_group_size
from fetch import kaggle_download
from load import (
    path_to_datasets, unzip, mk_dir, clean_up, stat_dir_or_file,
    load_csv_into_dataframe)
from load.pipeline import with_columns
from logger import ResearchLogger


class AdultCensusIncome(ProjectInterface):
    """
    Adult census data - predict individual income > 50K based on census data
    """
    _about = {
        'goal': 'Discover which census variables (e.g. race, education) '
                'are indicative of income greater than 50k',
        'description': """
            Using data from US Census Bureau 1994 Census, model
            classification task to classify individual
            based on income >50k or <= 50K.
           """,
        'conclusion': 'None so far'
    }

    DATA_DIR = path_to_datasets('adult-census-income')
    ARCHIVE = path_to_datasets('adult-census-income.zip')
    URL = 'https://www.kaggle.com/uciml/adult-census-income'
    DATA_URL = 'https://www.kaggle.com/uciml/adult-census-income/' \
               'downloads/adult-census-income.zip'
    CLS_COL = 'income_gr_50'

    def __init__(self):
        self.data_exists = stat_dir_or_file(self.DATA_DIR)
        self.archive_exists = stat_dir_or_file(self.ARCHIVE)
        self.visual = Visualizer(figsize=(18, 9))

    def fetch(self):
        print('Data from kaggle. Requires credentials...')
        username = input('Kaggle username: ')
        password = getpass.getpass('Kaggle password: ')
        kaggle_download(
            path_to_datasets(self.ARCHIVE), self.DATA_URL,
            UserName=username, Password=password
        )

    def unzip(self):
        unzip(self.ARCHIVE, mk_dir(self.DATA_DIR))

    def clean_up(self):
        return clean_up(self.DATA_DIR) and clean_up(self.ARCHIVE)

    def load(self, fetch=False):
        if fetch:  # Force fetching of data
            self.clean_up()
            self.fetch()
            self.unzip()

        if not self.data_exists:
            if not self.archive_exists:
                self.fetch()
            self.unzip()

        return True

    def load_csv(self, csv, **kwargs):
        return load_csv_into_dataframe(self.DATA_DIR, csv, **kwargs)

    def transform(self, df):
        final_columns = []
        # .pipe(with_columns, final_columns)
        return df.rename(columns=lambda c: c.replace('.', '_')) \
            .assign(**{self.CLS_COL: lambda df: df.income == '>50K'})

    def visualize(self, df):
        """
        Step through data visualizations
        :param df: dataframe
        """
        compare_with_income_cls_cols = [
            'race', 'sex', 'relationship', 'marital_status',
            'education', 'native_country', 'workclass', 'capital_gain']

        for col in compare_with_income_cls_cols:
            self.visual.close()
            print('CLOSE figure window to continue')
            self.compare_with_income_cls(df, col)

    def compare_with_income_cls(self, df, column):
        """
        Create stacked bar graph to compare variabel against
        classification of income >50K or not
        :param df: dataframe
        :param column: column str
        """
        self.visual.stacked_bar(
            count_group_size(
                df, [column, self.CLS_COL]
            ).unstack(level=1),
            xlabel=column,
            ylabel='count'
        ).show()
