from collections import OrderedDict

from load import path_to


class Countries(object):
    """
    Parser that is familiar with the structure of countries.csv
    """
    DIR = 'language/KeywordData/'
    FILE_NAME = 'countries.csv'

    ROW_SEP = '\n'
    COL_SEP = ','
    COL_OR = '|'

    def __init__(self):
        self.all_keywords = []
        self._data = {}
        self._buckets = {}
        self.parse(self.load()).bucket()

    @property
    def data(self):
        """
        Returns data sorted by key
        :return: OrderedDict
        """
        return OrderedDict(sorted(self._data.items(), key=lambda i: i[0]))

    @classmethod
    def load(cls):
        return open(path_to(cls.DIR, cls.FILE_NAME))

    @staticmethod
    def map_to_lowercase(l):
        return [a.lower() for a in l]

    def find(self, item):
        """
        Super dumb/inefficient search method
        :param cand: str
        :return: country name or None
        """
        bucket_key = item[0].lower()
        if bucket_key not in self._buckets:
            return None

        for c, is_c in self._buckets[bucket_key]:
            if is_c(item):
                return c
        return None

    def parse_row(self, row):
        """
        Parse country row, updates instance data dict
        :param row: str
        """
        parts = row.split(self.COL_SEP)
        raw_name = parts[0]
        name = '_'.join(raw_name.split(' ')).lower()
        adjs = self.map_to_lowercase(parts[1].split(self.COL_OR))
        abbrevs = [] if len(parts) <= 2 else self.map_to_lowercase(
            parts[2].split(self.COL_OR))

        self.all_keywords.extend([raw_name] + adjs + abbrevs)

        def _contains(cand):
            cand_lower = cand.lower()
            return (
                cand_lower == raw_name or
                cand_lower in adjs or
                cand_lower in abbrevs
            )

        self._data[name] = _contains

    def parse(self, raw_file):
        """
        Parse raw python file object for countries
        :param raw_file: file obj
        :return: map
        """

        # Mutates instance data
        [self.parse_row(r.strip())
         for r in raw_file.read().split(self.ROW_SEP)]

        return self

    def sort(self):
        self._data = OrderedDict(
            sorted(self._data.items(), key=lambda i: i[0]))
        return self

    def bucket(self):
        """
        Bucket keywords by first letter for somewhat faster search
        :return: {a: [(country, is_country<func>), ...], ...}
        """
        for c, is_c in self._data.items():
            if c[0] not in self._buckets:
                self._buckets[c[0]] = []
            self._buckets[c[0]].append((c, is_c))
        return self

