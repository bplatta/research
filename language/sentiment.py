from afinn import Afinn

# Overview of sentiment analysis methods
# https://pdfs.semanticscholar.org/ac3f/
# 8372b9d893dbdb7e4b9cd3df5ed825ffb548.pdf
# ?_ga=1.216580437.2105395394.1483158004


class LexicalBasedScorer(object):
    AFINN = 'afinn'
    _default = AFINN

    _types = {
        AFINN: lambda: Afinn()
    }

    def __init__(self, type=None):
        self.type = type or self._default
        self.scorer = None
        self.set_type(self.type)

    def set_type(self, t):
        if t not in self._types:
            raise Exception('Lexical scorer of type %s not supported' % t)
        self.scorer = self._types.get(t)()
        return self

    def score(self, text):
        return self.scorer.score(text)


def categorize_score(score):
    score_int = int(score)
    if score_int == 0:
        return 'neutral'
    elif score_int > 0:
        return 'positive'
    else:
        return 'negative'
