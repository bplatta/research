class DataLoadError(Exception):
    def __init__(self, message):
        self.message = message

    def __repr__(self):
        return 'DataLoadError: %s' % self.message
