# [Pandas](http://pandas.pydata.org/pandas-docs/stable) notes and terminology

#### Series
`pd.Series([] or {})` 
- represents Column with index. default index: 0 - N
- constructed with {}, keys used as indexes
###### Query series
- Selecting by index: `Series[{index}}` or `Series[{index}[]]`
- bool condition: `S[ S < 1000 ]` S < 1000 returns Series of Bools
- `series.notnull()` or `series.isnull()` filter by null value

###### Manipulation
- `Series \ 10` - applies / 10 to each value
- `np.square(series)` -  squares all values

#### Dataframe
`pd.DataFrame(data_obj, columns={str}[])`
- represents tabular data structure comprised of rows and columns or group of
Series object that share an index
- e.g. `data = {
    'year': [2010, 2011, 2012],
    'team': ['Bears', 'Packers', 'Bears'],
    'wins': [11, 8, 10]
}` and 
`columns = ['year', 'team', 'wins']`
- columns provides column order
- each list in data represents a column, order is maintained

###### Manipulation
- `frame.info()` - columns and types, memory usage
- `frame.describe()` - basic statistics about dataframe
- `frame.set_index('id')` - set dataframe index to a column (`reset_index` to get back)
- [querying](http://pandas.pydata.org/pandas-docs/stable/indexing.html)

`users[(users.age == 40) & (users.sex == 'M')].head(3)`

- can use `|` or `&` for multiple conditions
- select rows by position with `frame.iloc([1,2,3])`
- select rows by label with `frame.loc()`
- (merging)[http://pandas.pydata.org/pandas-docs/stable/merging.html#concatenating-objects]: `pd.merge(left_frame, right_frame, on='key', how='inner')`
- combining: `pd.concat([left_frame, right_frame], {axis=1})`

- sql-like:
 - `frame.groupby(col).size().sort_values(ascending=False)[:25]` or `frame.col.value_counts()`
 - aggregation: `frame.groupby(col).agg({col: [func, func, np.mean]})`
 
- binning data:
 - `frame.cut(lens.age, range(0, 81, 10), right=False, labels=labels)` (right=False - bins are exclusive)

- reorganizing data
 - unstack, unstacks the specified level of a MultiIndex (by default, groupby turns the grouped field into an index - since we grouped by two fields, it became a MultiIndex)
 - `frame.unstack(1)` unstacks second index
 - `frame.fillna(0)` fill NA values

### [IO](http://pandas.pydata.org/pandas-docs/stable/io.html)

`data = pd.read_csv(filename, sep=',', header=None, names=str[])`

`data.head()`

- accepts a `converters` argument - dict of functions, col -> func