## Data Exploration
- Variable Identification
 - identify predictor and target variables
 - identify data types and categories of variables
  - string, float?
  - continous? binary?
- Univariate Analysis
 - continous variable
  - what is the distribution of the variable and its main characteristics?
  - mean? spread? mode? median? quartiles?
  - histogram
 - categorical variable
  - frequency table, bar chart, counts, count percentages
- Bi-variate Analysis
 - continuous & continuous
  - scatter plot
  - linear? non-linear?
  - correlation of the variables? cov(x,y) / sqrt(var(x) * var(y))
   - there are different types of correlations mind you
 - categorical & categorical
  - 2-way table of count and count percentage
   - rows: category of 1 variable
   - columns: category of other variable
  - stacked column chart
  - X-squared test: stat significance between vars. Evidence in sample strong enough to generalize?
   - Chi-square is based on the difference between the expected and observed frequencies in one or more categories in the two-way table
   - returns prob for computed chi-square distribution with the degree of freedom
   - P = 0 - both cat variables dependent
   - P = 1 - variables independent
   - P < 0.05 - relationship between vars is significant at 95%
  - Other measures:
   - Cramers V for Nominal Cat Vars
   - Mantel-Haenszed Chi-Square for ordinal categorical variable
 - categorical & continous
  - box plots for each level of categorical variable
  - testing methods: Z-Test, T-Test, ANOVA
   - Z/T - assess whether mean of 2 groups are statistically different from each other
    - prob of Z is small, then diff of 2 averages is more significant
    - T test used when number of observations is less than 30
   - ANOVA - assess whether average of more than 2 groups is significantly different

- Missing values treatment
 - can reduce power or fit of model or lead to biased results
 - Why does this happen?
  - due to data extraction possibly
   - hashing procedures for verifying data?
  - at data collection time
   - missing completely at random - prob of missing variable same for all observations
   - missing random - missing ratio varies for different values / levels of other input variables
   - missing that depends on unobserved predictors - missing values not random, related to
     unobserved input variable
   - missing that depends on missing value - prob of missing variable is directly correlated with
     missing value itself
 - Dealing with missing values
  - deleting values - used when nature of missing data is 'missing completely at random'
   - list wise deletion - delete observation where any of the variable is missing. Reduces sample size
   - pair wise deletion - perform analysis with all the cases in which variables of interest are present. 
     Different sample size for different variables.
  - Mean/Mode/Median Imputation - fill in missing values with estimated ones
   - replacing missing variable with mean/median/mode (last being qualitative) of collected values
    - Generalized Imputation - replace with mean/median
    - Similar Case Imputation - calculate average for similar cases and fill in missing variables
      case by case
  - Prediction Model - create a predictive model for substituting the missing data
   - divide data set into 2 sets: One set with no missing values, and other with
    - first data set (w/o missing values) becomes training data while second set is test set
    - variable with missing values is target variable
    - can use regression, ANOVA, logistic regression etc.
    - drawbacks
     - The model estimated values are usually more well-behaved than the true values
     - This is assuming a correlation between the missing values and the other variables
       in the training set. That the latter could reliably predict them
  - KNN Imputation - missing values of observation imputed given number of observations
    that are most similar to observation who values are missing
    - similarity determined using some distance function
    - Advantages
     - K-nearest neighbor can predict both quantitative and qualitative attributes
     - creation of prediction model not required
     - attributes with multiple missing values can be easily treated
     - correlation of data taken into account
    - Disadvantage:
     - KNN algorithm is very time consuming for large data sets
     - choice of k-value is critical. higher value includes attributes that are significantly different

- Outlier treatment
 - Types of outliers
  - Univariate - found when looking at distribution of a single variable
  - multivariate - outliers in n-dim space
 - Ask: why do we have these outliers?
  - Artificial error - data entry, measurement error, experimental error (design), 
    intentional error (in reporting), data extraction/munging errors, 
    sampling error (wrong sample, some incorrect candidates)
  - Natural error - none of the above. really happened!
 - Increase error variance of data and power of statistical tests
 - if errors non-randomly distributed, can decrease normality
 - can bias or influence estimates
 - Can impact the basic assumption of regression, ANOVA, and other methods
 - How to detect?
  - box plot
  - histogram
  - scatter plot
  - other methods
   - look for values beyond range of -1.5 x IQR to 1.5 x IQR (inter quartile range)
   - using capping methods - values out of 5% and 95% percentiles
   - data points 3 or more std deviations from mean
   - multivariate usually measured using either index of influence or leverage, or distance:
     Mahalanobis or Cooks D
 - How to remove outliers?
  - deleting - data entry, data processing error, or small in number
  - binning - e.g. decision tree
  - transforming - natural log can reduce variation. can assign weights
  - treat as seperate group - if there are a lot
  - imputing values - mean/mode/median (only if artificial
  
- Feature Engineering
 - science and art of extracting more information from existing data (leverage!)
 - Variable transformation - replacement of variable by a funcion
  - x by sqrt(x)
  - changes distribution or relationship of variable with others
  - When?
   - change the scale of the variable or standardize the values of a variable
     for better understanding. While this transformation is a must if you have
     data on different scales, does not change shape of variable distribution
   - transforming complex non-linear relationships into linear relationships
    - log transformation common
   - symmetric distribution preferred over skewed distribution
    - some techniques requires normal distribution of variables
    - right skewed can use square or cube root or log
    - for left we can take square, cube or exponential
   - transformation done from an implementation point of view (human involvement)
  - How?
   - Log - common to change shape of distribution, reducing right skewness of variables
   - square/cube root - effect on variable distribution, not as significant as log. cube can 
     be applied to negative vals and 0, square root to postive values including 0
   - binning - used to categorize variables. Performed on original values, percentile or frequency
 - Variable creation - creating new variables based on existing variables
  - creating derived variables
   - using set of functions or different methods
   - e.g. using other variable to predict missing values
  - creating dummy variables - convert dummy variable into numerical variable
