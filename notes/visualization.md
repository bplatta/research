## Graphing

### Histogram
- distribution of a variable
- univariate analysis
- represents the distribution of data by forming bins along the range of the data 
and then drawing bars to show the number of observations that fall in each bin.
- with pandas: 
 - `df2.plot.hist()`
 - kwargs: stacked=bool, bins=int, cumulative=bool, orientation='horizontal' 

### Bar
- labeled, non-time series data
- with pandas: 
 - `df2.plot.bar()  # kwargs - stacked=Bool` 
 - `df2.plot.barh()  # kwargs - stacked=Bool` horizontal bar 
 
 
### Box Plot
- distribution of values within a column
- pandas:
 - `df.plot.box()`
 - kwargs:
  - color {boxes: str, whiskers: str, medians: str, caps: str}
 - `df.boxplot()`
 
### Area Plot
- asdf
- pandas:
 - `df.plot.area(stacked=bool)`
 
### Scatter Plot
- bivariate analysis
- pandas:
 - `plot.scatter(x='columnX', y='columnY')`
  - kwargs:
   - color: color of dots
   - label: str label for dots
  - multiple groups:
   - `ax = df.plot.scatter(...); df.plot.scatter(..., ax=ax)`
   
### Hexagonal Bin plot
- useful alternative to scatter plots if points are too dense
- pandas:
 - `df.plot.hexbin()`
 
### Pie plot
- pandas:
 - `df.plot.pie(figsize=(6,6))` - use square ratio
 
### Density plot
Kernel density estimation. Useful for plotting the shape of a distribution

- seaborn:
 - `sns.displot` will provide a density estimate
 - `sns.kdeplot(x, shade=bool)`
- pandas: `series.plot.kde()`

### Rug plot
a tick for each observation

- seaborn: `sns.rugplot()` or `sns.displot(rug=True)`