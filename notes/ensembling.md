## Ensembling multiple models

- Simple Majority Vote Ensemble
 - rely on averaging multiple signals
 - Makes sense when the evaluation metric requires tough predictions, for instance
   with multi-class classification accuracy
 - Weighted majority votes - giving a better model more weight in vote
  - thought being - the only way for inferior models to overrule the best model is for them
    to agree on an alternative
- Uncorrelated submissionss do better than correlated ones
- bagging submissions!
- averaging predictions reduces overfit
- Rank averaging
 - not all predictors are perfectly calibrated
  - calibration - Say 0.90 is predicted for a probability. We would want a sampling
    to exibit that trait 90% of the time
 - say it predicts similiar target values
 - turn prediction into ranks
 - normalize between 0 and 1
 - Historical ranks
  - ranking requires a test set, o what when you want predictions for single new sample?
  - store old test set preditions together with rank, now with new prediction, find the 
    closest old prediction and take its historical rank
 - rank averages do well on ranking and threshold based metrics (like AUC) and
   search engine quality metrics
- Stacked generalization
 - basic idea of stacked gen - use a pool of base classifiers, then use another one
   to combine their predictions, with the aim of reducing the generalization error
 - example of 2-fold stacking
  - take training set, divide into train_a and train_b
  - fit first stage model on train_a, create predictions for train_b
  - fit same model on train_b, create predictions for train_a
  - fit model on entire training set, create predictions for test set
  - train second stage stacker model on the probabilities from first stage models
 - gets more info by using the first-stage predictions as features
 - """It is usually desirable that the level 0 generalizers are of 
   all “types”, and not just simple variations of one another 
   (e.g., we want surface-fitters, Turing-machine builders, statistical 
   extrapolators, etc., etc.). In this way all possible ways of examining 
   the learning set and trying to extrapolate from it are being exploited. 
   This is part of what is meant by saying that the level 0 generalizers 
   should “span the space”." """
- Blending
 - similar to stacking, less complex, less likely to leak information
 - instead of creating 'out-of-fold' predictions for the train set, create a small 
   holdout set of 10% of train data
   - stacker model trains on this 10% data set only
 - benefits
  - simpler than stacking
  - wards against information leak: generalizers and stackers use different data
  - dont need to share a seed for stratified folds
 - cons
  - use less data overall
  - final model may overfit to holdout set
