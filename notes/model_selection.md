# Alternatives to least squares
- prediction accuracy, especially when # features > # samples. Need to control variance
- model interpretability - by removing irrelevant features, the found coefficients are more
interpretable

Methods:
- subset selection - determine subset of p features that are related to response
- shrinkage methods - add a regularization term that penalizes large coefficients
- dimension reduction - project p features into M dimensional subspace, where M < p

### Best subset selector
- consider M0 model that contains no predictors - predicts sample mean
- for all combinations of predictors, choose best model for x predictors (p choose x)
- then choose best model from M0 - Mp models
 - can use RSS (or R^2) - this doesnt take into account model complexity
  - deviance (-2 ML)
 - cross validation
 - adjusted R^2
 - can use BIC - baysian information criterion
- thats 2^p subsets !!!! oh lord

#### Stepwise selection!
- look at like p^2 models 1 + p(p + 1) / 2
- forward - start with model with no predictors
 - look for best predictor, add it -> M1
 - look at p - 1 predictors, choose best, add it -> M2
 - ...
- choose predictors with CV, BIC, AIC, adjusted R^2
- backward - start model with all predictors
 - need more observation than variables n > p
 
#### Estimating test error
- indirectly by computing training error and adjust
- directly estimate test error
- Cp, AIC, BIC, adjusted R^2 adjust training error for model size

#### Mallows Cp
Cp = 1/n (RSS + 2dx^2)
where d is # parameters and x^2 is estimate of the variance of the error

- usually estimate x^2 with all predictors, take mean square residual

#### AIC
Aikike information criteria `AIC = -2 log L` where L is Max value for likelihood function

- AIC and Cp are the same for linear models

#### BIC
Baysian Information criterion `BIC = 1/n (RSS + log(n) d v^2)`

- d number of predictors (with intercept), v is estimate of variance
- estimate avg test set RSS
- choose smallest BIC
- BIC tends to choose smaller models than AIC

#### Adjusted R^2
`Adj R^2 = 1 - (RSS/n - d - 1)) / (TSS/(n - 1))`

- want a LARGE value
- pay price for having large model
- can apply when p > n and dont need estimate of variance