## Information Theory notes

#### def : Cross Entropy (Wikipedia)
Between 2 probability distributions p and q, over the same underlying set of events
measures the number of bits needed to identify an event drawn from the set,
if a coding scheme is used to identify an 'unnatural' prob distribution q,
rather than the true distribution p