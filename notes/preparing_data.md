# Tidy Data
src: http://vita.had.co.nz/papers/tidy-data.pdf

- Difficulty: there are many ways to structure the a dataset
 - what is the best way for analysing?
- Data semantics
 - dataset is collection of values (value y at row i and col j)
 - Every value belongs to a variable and an observation
 - variable - contains all values that measure same underlying attribute
 - observation - all values measured on the same unit
- cleaning data
 - structural missing values, values that represent measurements that cant be made, can be dropped
- easier to describe functional relationships between variables than between rows
- easier to make comparisons between groups of observations than between groups of columns
- tidy data (Codds 3rd normal form):
 - each variable forms a column
 - each observation forms a row
 - each type of observational unit forms a table
- fixed variables should come first, then measured variables

Thoughts:
- this accords with wise relational db design as i see it.

# Preparing data
src: http://machinelearningmastery.com/how-to-prepare-data-for-machine-learning/